#  Copyright 2020 Dalton Pineault
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
#  License. You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "
#  AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific
#  language governing permissions and limitations under the License.

import discord

GUILD_ICON = "https://cdn.discordapp.com/app-icons/786681602482372639/18a0ff76c71e5003b85b9aab3a2e0c8a.png?size=256"


def success_embed(message):
    embed = discord.Embed(title="✅ Success ✅", colour=0x00ff00)
    embed.set_thumbnail(url=GUILD_ICON)
    embed.add_field(name="Description", value=message)
    embed.set_footer(text="Developed By @DaltonCodes")
    return embed


def error_embed(message):
    embed = discord.Embed(title="❗ Error ❗", colour=0xff0000)
    embed.set_thumbnail(url=GUILD_ICON)
    embed.add_field(name="Description", value=message)
    embed.set_footer(text="Developed By @DaltonCodes")
    return embed


def match_starting_embed(team_one, team_two):
    embed = discord.Embed(title="🏀 Match Starting 🏀", description='There is a match starting!', colour=0xfc7f03)
    embed.set_thumbnail(url=GUILD_ICON)
    embed.set_footer(text="Developed By @DaltonCodes")
    team_one_str = ""
    team_two_str = ""
    for player in team_one.players:
        team_one_str += f"{player.member.display_name} playing as {player.role}" + "\n"
    for player in team_two.players:
        team_two_str += f"{player.member.display_name} playing as {player.role}" + "\n"

    embed.add_field(name='Team One', value=team_one_str, inline=True)
    embed.add_field(name='Team Two', value=team_two_str, inline=True)

    return embed
