#  Copyright 2020 Dalton Pineault
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
#  License. You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "
#  AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific
#  language governing permissions and limitations under the License.

import config
import discord
from helpers import error_embed, success_embed


async def handle_messages(message, client):
    # Return if the bot sends a message
    if message.author == client.user:
        return

    # Change users nickname
    if message.channel.id == config.REGISTER_NICKNAME_CHANNEL_ID:
        try:
            # Set the nickname to whatever they've typed into the channel
            await message.author.edit(nick=message.content)
            new_message = await message.channel.send(embed=success_embed("Successfully changed your nickname"))
            # Delete both messages from the channel to keep it clean
            await message.delete()
            await new_message.delete(delay=5)
        # Handle the error where the member has higher permissions than the bot
        except discord.Forbidden:
            error_message = await message.channel.send(embed=error_embed(
                "I don't seem to have the correct permissions to change your nickname,"
                " you're either a server owner or have higher permissions than me. Please contact an Admin"))
            # Delete both messages from the channel to keep it clean
            await message.delete()
            await error_message.delete(delay=10)

    if message.channel.id == config.PROMOTION_CHANNEL_ID:
        pass

    if message.content == "test":
        await message.channel.send("hey")

    await client.process_commands(message)
