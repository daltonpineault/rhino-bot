#  Copyright 2020 Dalton Pineault
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
#  License. You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "
#  AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific
#  language governing permissions and limitations under the License.

import discord
import config


async def on_reaction_add(payload, client):
    message_id = payload.message_id
    emoji_name = payload.emoji.name
    # Check the reaction for which role / position the user is registering for
    if message_id == config.REGISTER_CONSOLE_MESSAGE_ID:
        console_role = None
        guild_id = payload.guild_id
        guild = discord.utils.find(lambda g: g.id == guild_id, client.guilds)

        if emoji_name == 'XBOX':
            console_role = discord.utils.get(
                guild.roles, id=config.XBOX_ROLE_ID)
        elif emoji_name == 'PS5':
            console_role = discord.utils.get(
                guild.roles, id=config.PS5_ROLE_ID)

        if console_role is not None:
            if payload.member is not None:
                await payload.member.add_roles(console_role)
            else:
                return
        else:
            print("Role not found")
            return
    # Check the console that the user is registering for
    if message_id == config.REGISTER_ROLE_MESSAGE_ID:
        role = None
        guild_id = payload.guild_id
        guild = discord.utils.find(lambda g: g.id == guild_id, client.guilds)

        if emoji_name == 'CENTER':
            role = discord.utils.get(guild.roles, id=config.CENTER_ROLE_ID)

        if emoji_name == 'CORNER':
            role = discord.utils.get(guild.roles, id=config.CORNER_ROLE_ID)

        if emoji_name == 'LOCK':
            role = discord.utils.get(guild.roles, id=config.LOCK_ROLE_ID)

        if emoji_name == 'HASH':
            role = discord.utils.get(guild.roles, id=config.HASH_ROLE_ID)

        if emoji_name == 'BALLHANDLER':
            role = discord.utils.get(
                guild.roles, id=config.BALL_HANDLER_ROLE_ID)

        if role is not None:
            if payload.member is not None:
                # Add the role to the user
                await payload.member.add_roles(role)
            else:
                print("Member not found")
        else:
            print("Role not found")


async def on_reaction_remove(payload, client):
    message_id = payload.message_id
    emoji_name = payload.emoji.name

    if message_id == config.REGISTER_CONSOLE_MESSAGE_ID:
        console_role = None
        guild_id = payload.guild_id
        # Get the build
        guild = discord.utils.find(lambda g: g.id == guild_id, client.guilds)

        if emoji_name == 'XBOX':
            console_role = discord.utils.get(
                guild.roles, id=config.XBOX_ROLE_ID)
        elif emoji_name == 'PS5':
            console_role = discord.utils.get(
                guild.roles, id=config.PS5_ROLE_ID)

        if console_role is not None:
            # Get the member object of the user reacting
            member = discord.utils.find(
                lambda m: m.id == payload.user_id, guild.members)
            if member is not None:
                await member.remove_roles(console_role)
            else:
                print("Member not found")
        else:
            print("Role not found")

    if message_id == config.REGISTER_ROLE_MESSAGE_ID:
        role = None
        guild_id = payload.guild_id
        guild = discord.utils.find(lambda g: g.id == guild_id, client.guilds)

        if emoji_name == 'CENTER':
            role = discord.utils.get(guild.roles, id=config.CENTER_ROLE_ID)

        if emoji_name == 'CORNER':
            role = discord.utils.get(guild.roles, id=config.CORNER_ROLE_ID)

        if emoji_name == 'LOCK':
            role = discord.utils.get(guild.roles, id=config.LOCK_ROLE_ID)

        if emoji_name == 'HASH':
            role = discord.utils.get(guild.roles, id=config.HASH_ROLE_ID)

        if emoji_name == 'BALLHANDLER':
            role = discord.utils.get(
                guild.roles, id=config.BALL_HANDLER_ROLE_ID)

        if role is not None:
            member = discord.utils.find(
                lambda m: m.id == payload.user_id, guild.members)
            if member is not None:
                # Remove the role from the user
                await member.remove_roles(role)
            else:
                print("Member not found")
        else:
            print("Role not found")
