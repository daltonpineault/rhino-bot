#  Copyright 2020 Dalton Pineault
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
#  License. You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "
#  AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific
#  language governing permissions and limitations under the License.

import os

token = os.getenv("RHINO_TOKEN")

RHINO_TWITTER_API_KEY = os.getenv("RHINO_TWITTER_API_KEY")
RHINO_TWITTER_API_KEY_SECRET = os.getenv("RHINO_TWITTER_API_KEY_SECRET")
RHINO_TWITTER_BEARER_TOKEN = os.getenv("RHINO_TWITTER_BEARER_TOKEN")
RHINO_TWITTER_ACCESS_TOKEN = os.getenv("RHINO_TWITTER_ACCESS_TOKEN")
RHINO_TWITTER_ACCESS_SECRET = os.getenv("RHINO_TWITTER_ACCESS_SECRET")

MAIN_GUILD_ID = 784849727937970176
# Messages
REGISTER_ROLE_MESSAGE_ID = 786707330480996373
REGISTER_CONSOLE_MESSAGE_ID = 786705905042849812

# Channels
REGISTER_ROLE_CHANNEL_ID = 784947225779372032
REGISTER_NICKNAME_CHANNEL_ID = 786705671697334292
RULES_CHANNEL_ID = 784946844316205056
PS5_QUEUE_LOBBY_CHANNEL_ID = 785283404807536700
DEV_CHANNEL_ID = 786677232432578590
MATCH_LOGS_CHANNEL_ID = 787821133709574207
PROMOTION_CHANNEL_ID = 784961379140501504

# Position / Player Roles
BALL_HANDLER_ROLE_ID = 784850855207632908
CORNER_ROLE_ID = 784850996261945375
CENTER_ROLE_ID = 784851937790918677
HASH_ROLE_ID = 784850931572277249
LOCK_ROLE_ID = 784850974376198154
XBOX_ROLE_ID = 784911903314542612
PS5_ROLE_ID = 784911779792158732
# Admin Roles
ADMIN_ROLE_ID = 784850848660324423
