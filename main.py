#  Copyright 2020 Dalton Pineault
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
#  License. You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "
#  AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific
#  language governing permissions and limitations under the License.

import discord
from discord.ext import commands
from twitter import *

import config
from commands import queue, admin
from register_reactions import on_reaction_add, on_reaction_remove
from message_handler import handle_messages

# twitter_api = Twitter(auth=OAuth(config.RHINO_TWITTER_ACCESS_TOKEN, config.RHINO_TWITTER_ACCESS_SECRET,
#                                  config.RHINO_TWITTER_API_KEY, config.RHINO_TWITTER_API_KEY_SECRET))


# Discord
client = commands.Bot(command_prefix=['rb=', '=', '-', '.', '?', 'rb-'], intents=discord.Intents.all())
MAIN_GUILD: discord.Guild = client.get_guild(config.MAIN_GUILD_ID)


@client.command(name='delete', aliases=['del', 'd', 'remove', 'r', 'purge'])
async def delete_messages(context, limit):
    roles = [role.id for role in context.author.roles]
    if config.ADMIN_ROLE_ID in roles:
        await admin.delete_messages(context, limit)
    else:
        await context.channel.send('No.')


@client.command(name='leave', aliases=['l'])
async def leave_queue(context):
    await queue.leave_queue(context)


@client.command(name='queue', aliases=['join', 'j', 'que', 'q', 'play'])
async def join_queue(context):
    await queue.join_queue(context)


@client.event
async def on_ready():
    print("Hello RhinoBot Is Ready")


@client.event
async def on_raw_reaction_add(payload):
    await on_reaction_add(payload, client)


@client.event
async def on_raw_reaction_remove(payload):
    await on_reaction_remove(payload, client)


@client.event
async def on_message(message):
    await handle_messages(message, client)


print('what?')
client.run(config.token)
