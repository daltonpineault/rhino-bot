# 🦏 Rhino Bot 🦏

## ❗ Description ❗

This is a Discord & Twitter bot for managing players in a 5v5 NBA-2k Queue.

## ❓ How-To ❓

* Reacting to the XBOX or PS5 emojis will grant you access to the discord
* Reacting to the Ball Handler, Lock, Hash, Center, Corner emojis will allow you to choose your role. Please be careful
  which you choose, these are roles you will play in game.
* Typing your name into the nickname-register channel will change your nickname
* Teams and roles are randomized
* Posting your YouTube.com or Twitch.tv channels into the self-promotion channel will send out a tweet on the bots
  Twitter advertising your channels.

## 🤖 Commands 🤖

* =q - join queue
* =l - leave queue