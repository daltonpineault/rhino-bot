#  Copyright 2020 Dalton Pineault
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
#  License. You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "
#  AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific
#  language governing permissions and limitations under the License.
import discord
import config
import random
from player import Player
from helpers import success_embed, error_embed, match_starting_embed
from team import Team

# Constants
MAX_PLAYERS_ALLOWED_IN_QUEUE = 2  # This should be 10
MAX_BALL_HANDLERS_ALLOWED = 1  # this should be 2
MAX_CENTERS_ALLOWED = 1  # this should be 2

# Queue information
playstation_queue = []
included_role_names = ['Ball Handler', 'Hash', 'Center', 'Corner', 'Lock']
ball_handler_count = 0
center_count = 0


async def leave_queue(context):
    member = context.author
    if context.channel.id == config.PS5_QUEUE_LOBBY_CHANNEL_ID:
        for player in playstation_queue:
            if member is player.member:
                playstation_queue.remove(player)
                await context.channel.send(embed=success_embed("You have left this queue."))
                return
        await context.channel.send(embed=error_embed("You are not in this queue."))


async def join_queue(context):
    global ball_handler_count, center_count
    member = context.author
    register_channel = discord.utils.get(context.guild.channels, id=config.REGISTER_ROLE_CHANNEL_ID)

    role_names = [role.name for role in member.roles if role.name in included_role_names]

    if not role_names:
        await context.channel.send(embed=error_embed(f"You must choose a role / position. Please go to"
                                                     f" {register_channel.mention}"))
        return

    random_role = random.choice(role_names)

    # Match is going to be ready once this player joins
    if len(playstation_queue) == MAX_PLAYERS_ALLOWED_IN_QUEUE - 1:
        player = Player(member, random_role)
        playstation_queue.append(player)

        team_one_players = []
        team_two_players = []

        for i, player in enumerate(playstation_queue):
            if i % 2 == 0:
                for t1_player in team_one_players:
                    if t1_player.role == 'Ball Handler':
                        # Theres already a ball handler on this team
                        print('already a ball handler on team one, putting them on team two')
                        team_two_players.append(player)
                        return
                    elif t1_player.role == 'Center':
                        # Theres already a ball handler on this team
                        print('already a center on team one, putting them on team two')
                        team_two_players.append(player)
                        return
                team_one_players.append(player)
            else:
                for t2_player in team_two_players:
                    if t2_player.role == 'Ball Handler':
                        # Theres already a ball handler on this team
                        print('already a ball handler on team two, putting them on team one')
                        team_one_players.append(player)
                        return
                    elif t2_player.role == 'Center':
                        # Theres already a ball handler on this team
                        print('already a center on team two, putting them on team one')
                        team_one_players.append(player)
                        return
                team_two_players.append(player)
        playstation_queue.clear()
        team_one = Team(team_one_players)
        team_two = Team(team_two_players)
        match_logs_channel = discord.utils.get(context.guild.channels, id=config.MATCH_LOGS_CHANNEL_ID)
        await match_logs_channel.send(embed=match_starting_embed(team_one, team_two))
        await context.channel.send(embed=success_embed(f"{member.display_name} has joined the Playstation Queue as a "
                                                       f"{random_role}"))
        await context.channel.send(embed=match_starting_embed(team_one, team_two))
        return

    if context.channel.id == config.PS5_QUEUE_LOBBY_CHANNEL_ID:
        roles = [role.id for role in member.roles]
        if config.PS5_ROLE_ID not in roles:
            if not register_channel:
                print("Error finding the register channel when checking a players role when joining queue")
                return
            await context.channel.send(embed=error_embed(f"You are not assigned to this console. Please go to"
                                                         f" {register_channel.mention} to select your role"))
            return

        for player in playstation_queue:
            if member is player.member:
                await context.channel.send(embed=error_embed("You are already in this queue."))
                return
            if random_role == 'Ball Handler':
                if ball_handler_count < MAX_BALL_HANDLERS_ALLOWED:
                    ball_handler_count += 1
                    player = Player(member, random_role)
                    playstation_queue.append(player)
                    await context.channel.send(
                        embed=success_embed(f"{member.display_name} has joined the Playstation Queue as a "
                                            f"{random_role}"))
                    return
                else:
                    random_role = random.choice(role_names)
                    if random_role == 'Ball Handler':
                        await context.channel.send(embed=error_embed(
                            "Theres already two Ball Handler's in this queue and i've tried a few times to randomize"
                            " your role, it seems you may only have 1 role selected. Please select at least 1 more role"
                            " or join the next queue."))
                        return
            elif random_role == 'Center':
                if center_count < MAX_CENTERS_ALLOWED:
                    center_count += 1
                    player = Player(member, random_role)
                    playstation_queue.append(player)
                    await context.channel.send(
                        embed=success_embed(f"{member.display_name} has joined the Playstation Queue as a "
                                            f"{random_role}"))
                    return
                else:
                    random_role = random.choice(role_names)
                    if random_role == 'Center':
                        await context.channel.send(embed=error_embed(
                            "Theres already two Center's in this queue and ive tried a few times to randomize your "
                            "role, it seems you may only have 1 role. Please select at least 1 more role or join the "
                            "next queue."))
                        return
        if random_role == 'Ball Handler':
            ball_handler_count += 1
        elif random_role == 'Center':
            center_count += 1
        # Player is joining as a hash, lock, or corner
        player = Player(member, random_role)
        playstation_queue.append(player)
        await context.channel.send(embed=success_embed(f"{member.display_name} has joined the Playstation Queue as a "
                                                       f"{random_role}"))
