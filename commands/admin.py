#  Copyright 2020 Dalton Pineault
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
#  License. You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "
#  AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific
#  language governing permissions and limitations under the License.

from helpers import error_embed


async def delete_messages(context, limit):
    try:
        number_limit = int(limit)
        async for message in context.channel.history(limit=number_limit):
            await message.delete()
    except ValueError:
        await context.channel.send(embed=error_embed("Please type in a number."))
